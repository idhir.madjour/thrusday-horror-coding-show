using System; 
using System.Linq; 

public namespace Inscriptions
{
    public enum TypeEtablissement
    {
        EcoleMaternelle, EcolePrimaire, College, Lycee, Universite
    }

    public abstract class Eleve
    {
        public int Id {get; set; }
        public string Nom {get; set;}
        public string Prenom {get; set;}
        public DateTime DateNaissace {get; set;}
    }

    public class EleveMaternelle : Eleve
    {
        ///<summary>
        /// Il faut bien acheter de quoi occuper nos petites têtes blondes
        ///</summary>
        public int AchatMaterielPeinture()
        {
            return 666;
        }
    }

    public class Ecolier : Eleve
    {
        ///<summary>
        /// C'est totalement arbitraire mais un enfant coute environ 777 €/an en bonbon. J'ai fait le calcul moi même à neverland
        ///</summary>
        public int AchatBonbon()
        {
            return 777;
        }
    }

    public class Collegien: Eleve
    {

        ///<summary>
        /// De mon temps on lisait les dragon ball, maintenant c'est quoi ce Borito ??? 
        /// Quoi je le prononce mal? Boruto fils de Naruto. 1000€ /an de manga. 
        ///</summary>
        public int AchatManga()
        {
            return 999;
        }
    }


    public static class Inscription
    {
        ///<summary>
        /// 
        ///</summary>
        public static int CalculerFraisInscription(Eleve eleve, bool estInstitutionPrivee)
        {
            if (eleve is EleveMaternelle)
            {
                var eleveMaternelle = eleve as EleveMaternelle;
                return FraisInscriptionEleveMaternelle(eleveMaternelle, estInstitutionPrivee);
            }

            if (eleve is Ecolier)
            {
                var ecolier = eleve as Ecolier;
                return FraisInscriptionEcolier(ecolier);
            }

            if (eleve is Collegien)
            {
                var collegien = eleve as Collegien;
                return FraisInscriptionCollegien(collegien);
            }

            throw new NotImplementedException("Je suis dans aucun des cas.");
        }

        public static int FraisInscriptionEleveMaternelle(EleveMaternelle eleveMaternelle, bool estUneEcolePrivee)
        {
            var surplus = (estUneEcolePrivee) ? 100 : 0;
            return 389 + surplus;
        }

        public static int FraisInscriptionEcolier(Ecolier ecolier) 
        {
            return 390;
        }

        public static int FraisInscriptionCollegien(Collegien collegien, bool aUneActiviteExtra)
        {
            var surplus = (aUneActiviteExtra) ? 200 : 0;
            return 763 + surplus;
        }
    }

}